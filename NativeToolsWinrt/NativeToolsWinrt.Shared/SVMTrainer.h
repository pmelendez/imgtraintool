#pragma once
#include "svm.h"
#include "TrainRect.h"
#include "ImageTrainCollection.h"
#include <windows.ui.xaml.media.dxinterop.h>
#include <collection.h>
#include <ppltasks.h>
#include <wrl/client.h>

namespace NativeToolsWinrt
{
	public ref class SVMTrainer sealed
	{
	private:
		struct svm_parameter param;		// set by parse_command_line
		struct svm_problem prob;		// set by read_problem
		struct svm_model *model;
	public:
		SVMTrainer() {};
		void Test() ;
		void Train(Platform::String^ BasePath, ImageTrainCollection^ points);
	};

}