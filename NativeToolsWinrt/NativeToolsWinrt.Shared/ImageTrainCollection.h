#pragma once

#include <unordered_map>
#include <vector>
#include <TrainRect.h>
#include <collection.h>
#include <random>

namespace NativeToolsWinrt
{
	public ref class ImageTrainCollection sealed
	{
	private:
		std::unordered_map<std::string, std::vector<TrainRect^>> points;

		std::string PStrToStdStr(Platform::String^ PStr)
		{
			auto wchr = PStr->Data();
			std::wstring ws(wchr);
			std::string str(ws.begin(), ws.end());
			return str;
		}

		TrainRect^ IsPointInsideARect(Windows::Foundation::Point p, std::string file)
		{
			for(auto rect : points[file])//foreach(var rect in points.GetPoints(file))
			{
				if (rect->IsPointInside(p))
				{
					return rect;
				}
			}

			return nullptr;
		}

	public:
		void AddPoint(TrainRect^ Point, Platform::String^ ImageName)
		{
			auto str = PStrToStdStr(ImageName);
			points[str].push_back(Point);
		}

		void RemovePoint(TrainRect^ Point, Platform::String^ ImageName)
		{
			auto str = PStrToStdStr(ImageName);
			points[str].erase(std::remove(points[str].begin(), points[str].end(), Point), points[str].end());
		}

		TrainRect^ GetPoint(Platform::String^ ImageName, int Position)
		{
			auto str = PStrToStdStr(ImageName);			
			return points[str][Position];
		}

		void AddKey(Platform::String^ currentFile)
		{
			auto str = PStrToStdStr(currentFile);
			// PM@Hack This force the vector object to be created
			points[str];
		}

		bool ContainsKey(Platform::String^ ImageName)
		{
			auto str = PStrToStdStr(ImageName);
			return points.find(str) != points.end();
		}

		Windows::Foundation::Collections::IVector<TrainRect^>^ GetPoints(Platform::String^ filename)
		{
			auto str = PStrToStdStr(filename);
			auto res = ref new Platform::Collections::Vector<TrainRect^>(points[str]);
			return res;
		}

		void GenerateNegatives(float ratio, float translatedRectSize, Windows::Foundation::Rect pickingArea, int number_negative_examples, Windows::Foundation::Point offset)
		{
			for(auto& rects : points)//foreach(var rects in points)
			{
				std::random_device rd;

				// Choose a random mean between 1 and 6
				std::default_random_engine e1(rd());
				std::uniform_int_distribution<int> rndX((int) pickingArea.X, (int) pickingArea.Width);
				std::uniform_int_distribution<int> rndY((int) pickingArea.Y, (int) pickingArea.Height);

				//PM@TODO
				//var rnd = new Random();
				
				for (int i = 0; i < number_negative_examples; i++)
				{
					int x = rndX(e1);//rnd.Next((int)pickingArea.X, (int)pickingArea.Width);
					int y = rndY(e1);//rnd.Next((int)pickingArea.Y, (int)pickingArea.Height);
					auto newPoint = Windows::Foundation::Point(x + offset.X, y + offset.Y);

					if (IsPointInsideARect(newPoint, rects.first) == nullptr)
					{
						auto scaledPoint = Windows::Foundation::Point((newPoint.X * (1 / ratio)), (newPoint.Y * (1 / ratio)));
						auto TrainPoint = ref new TrainRect(scaledPoint, translatedRectSize, TrainClass::negative, ratio);

						rects.second.push_back(TrainPoint);
					}

				}
			}
		}

	};
}


