#pragma once

namespace NativeToolsWinrt
{
	public enum class TrainClass
	{
		positive,
		negative
	};

	public ref class TrainRect sealed
	{
	private:
		TrainRect()
		{

		}

		Windows::Foundation::Point GetCorner()
		{
			return Windows::Foundation::Point((float)(Center.X - Size / 2), (float)(Center.Y - Size / 2));
		}
	public:
		TrainRect(Windows::Foundation::Point p, float rectSize, TrainClass target, float scaleRatio)
		{
			Center = p;
			Size = rectSize;
			Target = target;
			ScaleRatio = scaleRatio;
		}

		Windows::Foundation::Rect GetRect()
		{
			return Windows::Foundation::Rect(Center, Windows::Foundation::Size(Size, Size));
		}


		Windows::Foundation::Rect GetScaledRect()
		{
			auto scaledSize = Windows::Foundation::Size(Size * ScaleRatio, Size * ScaleRatio);
			auto Corner = GetCorner();
			auto scaledCorner = Windows::Foundation::Point(Corner.X * ScaleRatio, Corner.Y * ScaleRatio);
			return Windows::Foundation::Rect(scaledCorner, scaledSize);
		}
		
		bool IsPointInside(Windows::Foundation::Point unscaledPoint)
		{
			bool result = false;

			// The point is not scaled to the original image proportions so let's fix that
			auto scaledPoint = Windows::Foundation::Point(unscaledPoint.X * 1 / ScaleRatio, unscaledPoint.Y * 1 / ScaleRatio);
			auto corner = GetCorner();

			if (scaledPoint.X >= corner.X &&
				scaledPoint.X <= corner.X + Size &&
				scaledPoint.Y >= corner.Y &&
				scaledPoint.Y <= corner.Y + Size)
			{
				result = true;
			}

			return result;
		}

		Windows::UI::Color GetColor()
		{
			auto res = Windows::UI::ColorHelper::FromArgb(255, 50, 239, 10);

			if (Target == TrainClass::negative)
				res = Windows::UI::ColorHelper::FromArgb(255, 199, 10, 20);

			return res;
		}
		
		property Windows::Foundation::Point Center;
		property float Size;
		property float ScaleRatio;
		property TrainClass Target;


	};

}
