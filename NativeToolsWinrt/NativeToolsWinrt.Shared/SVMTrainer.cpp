#include "SVMTrainer.h"


using namespace NativeToolsWinrt;

void SVMTrainer::Test()
{
	
	int a = 0;

	// default values
	param.svm_type = C_SVC;
	param.kernel_type = RBF;
	param.degree = 3;
	param.gamma = 0.05;	// 1/num_features
	param.coef0 = 0;
	param.nu = 0.5;
	param.cache_size = 100;
	param.C = 1;
	param.eps = 1e-3;
	param.p = 0.1;
	param.shrinking = 1;
	param.probability = 0;
	param.nr_weight = 0;
	param.weight_label = NULL;
	param.weight = NULL;



	//Length
	prob.l = 4;                             //number of training examples


											//x values

	svm_node** x = new svm_node *[prob.l];  //Array of pointers to pointers to arrays

	svm_node* x_space1 = new svm_node[3];   //Fist training example
	svm_node* x_space2 = new svm_node[3];   //Second training example
	svm_node* x_space3 = new svm_node[3];   //Third training example
	svm_node* x_space4 = new svm_node[3];   //Fourth training example

	x_space1[0].index = 1;                  //Fist training example
	x_space1[0].value = 1;
	x_space1[1].index = 2;
	x_space1[1].value = 1;
	x_space1[2].index = -1;

	x_space2[0].index = 1;                  //Second training example
	x_space2[0].value = 1;
	x_space2[1].index = 2;
	x_space2[1].value = 0;
	x_space2[2].index = -1;

	x_space3[0].index = 1;                  //Third training example
	x_space3[0].value = 0;
	x_space3[1].index = 2;
	x_space3[1].value = 1;
	x_space3[2].index = -1;

	x_space4[0].index = 1;                  //Fourth training example
	x_space4[0].value = 0;
	x_space4[1].index = 2;
	x_space4[1].value = 0;
	x_space4[2].index = -1;

	x[0] = x_space1;                        //Set each training example to x
	x[1] = x_space2;
	x[2] = x_space3;
	x[3] = x_space4;

	prob.x = x;                             //Assign x to the struct field prob.x


											//yvalues
	prob.y = new double[prob.l];
	prob.y[0] = -1;
	prob.y[1] = 1;
	prob.y[2] = 1;
	prob.y[3] = -1;

	model = svm_train(&prob, &param);

	svm_node* testnode = new svm_node[3];
	testnode[0].index = 1;
	testnode[0].value = 1;
	testnode[1].index = 2;
	testnode[1].value = 0;
	testnode[2].index = -1;

	double retval = svm_predict(model, testnode);


	
}

void NativeToolsWinrt::SVMTrainer::Train(Platform::String ^ BasePath, ImageTrainCollection^ points)
{
	//throw gcnew System::NotImplementedException();
	int a = 0;
}


