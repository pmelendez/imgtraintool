﻿#pragma once

namespace NativeToolsWinrt
{
    public ref class FolderHelper sealed
    {
    public:
		FolderHelper();
		static Windows::Foundation::IAsyncOperation<uint32>^ GetFileCountWin32Async(Platform::String^ path);
		static uint32 GetFileCountWin32(Platform::String^ path);
    };
}
