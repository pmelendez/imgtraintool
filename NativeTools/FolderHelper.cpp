﻿#include "pch.h"
#include "FolderHelper.h"

using namespace NativeToolsWinrt;
using namespace Platform;

FolderHelper::FolderHelper()
{
	
}
uint32 FolderHelper::GetFileCountWin32(Platform::String^ path)
{
	std::wstring localFolderPath(path->Data()); //Windows::Storage::ApplicationData::Current->LocalFolder->Path
	localFolderPath += L"\\*";
	uint32 found = 0;
	WIN32_FIND_DATA findData{ 0 };
	HANDLE hFile = FindFirstFileEx(localFolderPath.c_str(), FindExInfoBasic, &findData, FindExSearchNameMatch, nullptr, FIND_FIRST_EX_LARGE_FETCH);

	if (hFile == INVALID_HANDLE_VALUE)
		throw ref new Platform::Exception(GetLastError(), L"Can't FindFirstFile");
	do
	{
		if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
			++found;
	} while (FindNextFile(hFile, &findData) != 0);

	auto hr = GetLastError();
	FindClose(hFile);
	if (hr != ERROR_NO_MORE_FILES)
		throw ref new Platform::Exception(hr, L"Error finding files");

	return found;
}

Windows::Foundation::IAsyncOperation<uint32>^ FolderHelper::GetFileCountWin32Async(Platform::String^ path)
{
	return concurrency::create_async([path]
	{
		return GetFileCountWin32(path);
	});
}