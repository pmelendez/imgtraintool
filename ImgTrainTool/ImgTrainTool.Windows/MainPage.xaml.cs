﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.Brushes;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using NativeToolsWinrt;
using Microsoft.Graphics.Canvas.DirectX;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace ImgTrainTool
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Setup();            
        }

        private bool updatingPoints;
        private static float RectSize = 20;
        private Random rnd = new Random();
        private Rect visibleRect = new Rect(0, 0, RectSize, RectSize);
        private Point offset = new Point(250, 0);
        //private Dictionary<string, List<TrainRect>> points = new Dictionary<string, List<TrainRect>>();
        private ImageTrainCollection points = new ImageTrainCollection();
        private double DrawableHeight;
        private Windows.UI.Color rectColor = Windows.UI.Color.FromArgb(255, 199, 239, 207);
        SVMTrainer svm;

        private void Setup()
        {
            svm = new SVMTrainer();
            /*svm.Test();*/
        }

        private float GetRndF()
        {
            return (float)rnd.NextDouble();
        }
        
        private void CanvasAnimatedControl_Draw(Microsoft.Graphics.Canvas.UI.Xaml.ICanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedDrawEventArgs args)
        {
            if(image != null)
            {
                if(!updatingPoints)
                {
                    try
                    {
                        var s = image.SizeInPixels;
                        DrawableHeight = (double)canvasAnim.Size.Height - 100;
                        double ratio = DrawableHeight / (double)image.SizeInPixels.Height;
                        var destR = new Rect(offset.X, offset.Y, (image.SizeInPixels.Width) * ratio, (DrawableHeight));
                        
                        args.DrawingSession.DrawImage(image, destR, image.Bounds);
                        
                        if (points.ContainsKey(currentFile))
                        {
                            var pointsPerFile = points.GetPoints(currentFile);
                            foreach (var p in pointsPerFile)
                            {
                                var newRect = p.GetScaledRect();
                                args.DrawingSession.DrawRectangle(newRect, p.GetColor());
                            }
                        }

                        //args.DrawingSession.DrawText("Hello world", GetRndF() * 100, GetRndF() * 100, imageBrush,new Microsoft.Graphics.Canvas.Text.CanvasTextFormat());
                    }
                    catch (Exception e)
                    {
                        int a = 0;
                    }
                }
            }
        }
        
        ICanvasResourceCreator rcreator;
        StorageFolder workFolder;
        string currentFile = "";
        CanvasBitmap image = null;
        uint currentItem = 0;

        private void LoadResourcesAsync(Microsoft.Graphics.Canvas.ICanvasResourceCreator sender)
        {
            rcreator = sender;
            canvasAnim.Input.PointerPressed += Input_PointerPressed;
            tbWindowSize.Text = RectSize.ToString();
        }

        private async Task LoadImages()
        {
            if(workFolder != null)
            {
                var folderQ = workFolder.CreateFolderQuery(Windows.Storage.Search.CommonFolderQuery.DefaultQuery);
                var items = await workFolder.GetFilesAsync(Windows.Storage.Search.CommonFileQuery.DefaultQuery, currentItem, 20);

                if(items.Count ==0)
                {
                    currentItem = 0;
                }

                // This foreach loop actually looks for the first "drawable" image. If it fails loading then it tries with the next one. 
                foreach (var fileElement in items)
                {
                    try
                    {
                        var stream = await fileElement.OpenStreamForReadAsync();
                        image = await CanvasBitmap.LoadAsync(rcreator, stream.AsRandomAccessStream());
                        currentFile = fileElement.Name;

                        if(!points.ContainsKey(currentFile))
                        {
                            points.AddKey(currentFile);
                            //points[currentFile] = new List<TrainRect>();
                        }/**/

                        return;
                    }
                    catch (Exception e)
                    {
                        int a = 0;
                    }
                }
            }

        }

        private void Input_PointerPressed(object sender, Windows.UI.Core.PointerEventArgs args)
        {
            var newPoint = args.CurrentPoint.Position;
            DrawableHeight = (double)canvasAnim.Size.Height - 100;
            float ratio = (float) (DrawableHeight / (double)image.SizeInPixels.Height);

            if (!IsPointInRect(newPoint, new Rect(offset, new Size(image.SizeInPixels.Width * ratio, DrawableHeight))))
                return;

            if(points.ContainsKey(currentFile) && image != null)
            {

                
                var SelectedRect = IsPointInsideARect(newPoint, currentFile);
                if (SelectedRect == null)
                {
                    var translatedPoint = new Point((newPoint.X - offset.X) * 1 / ratio, (newPoint.Y - offset.Y) * 1 / ratio);

                    visibleRect = new Rect(-1, -1, 1, 1);

                    if (args.CurrentPoint.Properties.IsLeftButtonPressed)
                    {
                        float translatedRectSize = RectSize * 1 / ratio;
                        visibleRect = new Rect(translatedPoint.X - translatedRectSize / 2, translatedPoint.Y - translatedRectSize / 2, translatedRectSize, translatedRectSize);

                        // Let's scale the rect that represents a window in the original image.
                        var scaledPoint = new Point(newPoint.X * (1 / ratio), newPoint.Y * (1 / ratio));
                        var TrainPoint = new TrainRect(scaledPoint, translatedRectSize, TrainClass.positive, ratio);
                        points.AddPoint(TrainPoint, currentFile); //[currentFile].Add(TrainPoint);
                    }
                }
                else
                {
                    if (args.CurrentPoint.Properties.IsLeftButtonPressed)
                    {
                        SelectedRect.Target = SelectedRect.Target == TrainClass.positive ? TrainClass.negative : TrainClass.positive;
                    }
                    else
                    {
                        // Pressed right click... if the point is inside of any rect, we would delete it
                        points.RemovePoint(SelectedRect, currentFile);
                        //points[currentFile].Remove(SelectedRect);
                    }
                }
                
            }
            
            args.Handled = false;
        }

        private TrainRect IsPointInsideARect(Point p, string file)
        {
            foreach (var rect in points.GetPoints(file))
            {
                if(rect.IsPointInside(p))
                {
                    return rect;
                }             
            }

            return null;
        }

        private bool IsPointInRect(Point p, Rect r)
        {
            bool result = false;
            if(p.X > r.X && p.X < r.X+r.Width &&
                p.X > r.Y && p.Y < r.Y + r.Height)
            {
                result = true;
            }

            return result;
        }

        private void CanvasAnimatedControl_CreateResources(Microsoft.Graphics.Canvas.UI.Xaml.CanvasAnimatedControl sender, Microsoft.Graphics.Canvas.UI.CanvasCreateResourcesEventArgs args)
        {
            LoadResourcesAsync(sender);
            //args.TrackAsyncAction(LoadResourcesAsync(sender).AsAsyncAction());
        }

        private void StartUpdating()
        {
            updatingPoints = true;
            canvasAnim.Paused = true;
        }

        private void StopUpdating()
        {
            updatingPoints = false;
            canvasAnim.Paused = false;
        }

        private void GenerateNegatives()
        {
            StartUpdating();
            try
            {
                var drawableHeight = (double)canvasAnim.Size.Height - 100;
                float ratio = (float)(drawableHeight / (double)image.SizeInPixels.Height);
                var drawableWidth = image.SizeInPixels.Width * ratio;
                float translatedRectSize = RectSize * 1 / ratio;

                var marginWPerc = 0.2;
                var marginHPerc = 0.1;
                var pickingArea = new Rect(drawableWidth* marginWPerc, drawableHeight* marginHPerc, drawableWidth - (drawableWidth * (marginWPerc)), drawableHeight - (drawableHeight * (marginHPerc)));
                int number_negative_examples = 500;

                points.GenerateNegatives(ratio, translatedRectSize, pickingArea, number_negative_examples, offset);

            }
            catch (Exception e)
            {
                int a = 0;
            }
            
            StopUpdating();
        }

        private async Task LoadFolder()
        {
            var folderPicker = new Windows.Storage.Pickers.FolderPicker();
            folderPicker.FileTypeFilter.Add(".jpeg");
            folderPicker.FileTypeFilter.Add(".jpg");
            folderPicker.FileTypeFilter.Add(".png");
            
            try
            {
                workFolder = await folderPicker.PickSingleFolderAsync();

                var progressRing = new ProgressRing();
                myGrid.Children.Add(progressRing);
                progressRing.Width = 300;
                progressRing.Height = 300;

                progressRing.IsActive = true;
                await LoadImages();
                progressRing.IsActive = false;

                myGrid.Children.Remove(progressRing);

                left.Visibility = Visibility.Visible;
                right.Visibility = Visibility.Visible;
            }
            catch(Exception e)
            {
                int a = 0;
            }

        }

        private async void OpenFolder_Tapped(object sender, TappedRoutedEventArgs e)
        {
            await LoadFolder();
        }

        private async void left_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                if (currentItem == 0)
                    return;

                currentItem--;
                await LoadImages();
            }
            catch(Exception ex)
            {
                currentItem = 0;
            }
            
        }

        private async void right_Tapped(object sender, TappedRoutedEventArgs e)
        {
            try
            {
                currentItem++;
                await LoadImages();
            }
            catch(Exception ex)
            {
                currentItem -= 2;
            }
            
        }

        private void resizeWindow_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if(currentFile !="")
            {
                ParentedPopup.IsOpen = true;
            }            
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            ParentedPopup.IsOpen = false;
        }

        private void mySlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if(tbWindowSize != null)
            {
                RectSize = (float)e.NewValue;
                tbWindowSize.Text = RectSize.ToString();
            }            
        }

        private void GenerateNegativeExamples_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GenerateNegatives();
        }

        private void Train_Tapped(object sender, TappedRoutedEventArgs e)
        {
            svm.Train(workFolder.Path, points);
        }
    }
}
